<?php
namespace Snitches\Attribute;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use League\Pipeline\PipelineBuilder;
use Snitches\Scrapers\SearchSource;

class ScrapeAttributeValue
{
  private $payload;

  private $initialScrapeValue;

  private $pipeline;

  public function __construct($payload)
  {
    $this->payload = $payload;
  }

  public function scrape()
  {
    if($this->payload->scraper->handler_type === 'class' && class_exists($this->payload->scraper->scraper)) {
      $this->initialScrapeValue = (new $this->payload->scraper->scraper)($this->payload);
    }

    if($this->payload->scraper->handler_type === 'console') {
      $this->initialScrapeValue = console_cmd($this->payload->scraper_payload);
    }
    
    $results = $this->scrapingPipeline();

    return $results instanceof Collection ? $results->toArray() : $results;
  }

  public function scrapingPipeline()
  {
    $this->pipeline = new PipelineBuilder();
    collect($this->payload->secondary_scrapers)
      ->sortBy('sort')
      ->each(function($secondary) {
        $this->pipeline->add(
          new $secondary->handler($secondary, $this->initialScrapeValue)
        );
    });

    $pipeline = $this->pipeline->build();

    return $pipeline->process(
      $this->payload->scraper->scraper === SearchSource::class ? [] : $this->initialScrapeValue
    );
  }


}