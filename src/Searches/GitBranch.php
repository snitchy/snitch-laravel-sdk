<?php

namespace Snitches\Searches;

use Exception;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class GitBranch
{

  public function __invoke()
  {
    $process = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
    $process->run();
    if ($process->isSuccessful()) {
      return $process->getOutput();
    } else {
      return null;
    } 
  }
}
