<?php

namespace Snitches\Searches;

use Exception;
use Symfony\Component\Process\Process;

class SoftwareExists
{
  private $software;

  private $exists = false;

  private $exists_at;

  public function __construct(string $software)
  {
    $this->software = $software;
  }

  public function __invoke($scan)
  {
    try {
      $scan->line('Checking if ' . $this->software . ' is installed');
      $process = new Process(['which'], $this->software);
      $process->run();
      if ($process->isSuccessful()) {
        $scan->line($process->getOutput());
      } else {
        $scan->line('Things did not go well');
      }
    } catch(Exception $exception) {
      $scan->line('Things really did not go well');
    }
  }
}