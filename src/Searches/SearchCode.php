<?php

namespace Snitches\Searches;

use Symfony\Component\Finder\Finder;

class SearchCode
{
  private $finder;

  private $results;

  private $search;

  private $basePath;

  public function __construct($search)
  {
    $this->finder = new Finder();
    $this->search = $search;
  }

  public function search()
  {
    $this->results = collect();

    $this->finder->files()
      ->name('*.php')
      ->in($this->basePath ?? base_path())
      ->contains($this->search);

    foreach ($this->finder as $file) {
      $this->results->push($file->getRealPath());
    }
 
    return $this->results;
  }

  public function setBasePath($path)
  {
    $this->basePath = $path;

    return $this;
  }

}