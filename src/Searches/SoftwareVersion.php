<?php

namespace Snitches\Searches;

use Exception;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class SoftwareVersion
{
  private $software;

  private $scan;

  private $versionIdentifier;

  public function __construct(string $software, string $versionIdentifier = '-v')
  {
    $this->software = $software;
    $this->versionIdentifier = $versionIdentifier;
  }

  public function __invoke($scan)
  {
    $this->scan = $scan;
    try {
      $this->scan['cmd']->comment("Checking for $this->software"); 
      $process = new Process(['which', $this->software]);
      $process->run();
      if ($process->isSuccessful()) {
        $this->scan['results']->push([
          'type' => 'software',
          'search' => $this->software,
          'output' => $this->getVersion(trim($process->getOutput()))
        ]);
      } else { 
        $this->scan['cmd']->line($this->software . ' is not installed');
      }
    } catch (Exception $exception) {
      $this->scan['cmd']->line($this->software . ' is not installed');
    }

    return $this->scan;
  }

  private function getVersion(string $pathToSoftware = null)
  {
    try {
      $process = new Process([$pathToSoftware ?? $this->software, $this->versionIdentifier]);
      $process->run();
      if ($process->isSuccessful()) {
        $this->scan['cmd']->line($this->software . ' is at version ' . $process->getOutput());
        return $process->getOutput();
      }
      return null;
    } catch (Exception $exception) {
      $this->scan['cmd']->line($exception);
      return null;
    }
  }
}
