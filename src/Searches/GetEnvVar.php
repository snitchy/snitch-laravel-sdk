<?php

namespace Snitches\Searches;

class GetEnvVar
{
  private $envVariable;

  public function __construct($envVariable) 
  {
    $this->envVariable = $envVariable;
  }

  public function __invoke($output)
  {
    $var = env($this->envVariable->name);
    $output['command']->line($this->envVariable->name .'='. $var);

    $output['results'][] = [
      'attribute' => $this->envVariable->id,
      'value' => $var
    ];

    return $output;
  }
}
