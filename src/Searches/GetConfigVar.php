<?php

namespace Snitches\Searches;

use Illuminate\Support\Facades\Config;

class GetConfigVar
{
  private $configVariable;

  public function __construct($configVariable)
  {
    $this->configVariable = $configVariable;
  }

  public function __invoke($output)
  {
    
    $var = config($this->configVariable->name);
    $output['command']->line($this->configVariable->name . '=' . $var);

    $output['results'][] = [
      'attribute' => $this->configVariable->id,
      'value' => $var
    ];

    return $output;
  }
}
