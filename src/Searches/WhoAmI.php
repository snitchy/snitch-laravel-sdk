<?php

namespace Snitches\Searches;

use Symfony\Component\Process\Process;

class WhoAmI
{

  public function __invoke($scan)
  {
    try {
      $process = new Process(['whoami']);
      $process->run();
      if ($process->isSuccessful()) {
        $scan['cmd']->comment('Process running as');
        $scan['cmd']->line($process->getOutput());
        $scan['results']->push([
          'type' => 'os',
          'search' => 'system_user',
          'output' => $process->getOutput()
        ]);
      }

      return $scan;
    } catch(\Exception $e) {
      return $scan;
    }
  }
}
