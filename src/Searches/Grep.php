<?php

namespace Snitches\Searches;

use Symfony\Component\Process\Process;

class Grep
{
  private $vulnerability;

  public function __construct($vulnerability)
  {
    $this->vulnerability = $vulnerability;
  }

  public function __invoke()
  {
    $process = Process::fromShellCommandline($this->vulnerability->handler);
    $process->run();

    return [
      'id' => $this->vulnerability->id,
      'value' => $process->getOutput()
    ];
  }
}
