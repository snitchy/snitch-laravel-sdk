<?php

namespace Snitches\Searches;

use Illuminate\Support\Facades\App;

class LaravelVersion
{

  public function __invoke()
  {
    return App::version();
  }
}
