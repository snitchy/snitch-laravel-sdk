<?php

namespace Snitches\Facades;

use Illuminate\Support\Facades\Facade;

class Snitch extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'snitch';
  }
}
