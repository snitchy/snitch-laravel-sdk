<?php

namespace Snitches\Schedule;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Config;

class Scheduler
{
  private $time;

  public function __construct()
  {
    $this->time = Config::get('snitch.scan_time');
    if ($this->time) {
      $this->run();
    }
  }

  public function run()
  {
    return app(Schedule::class)
      ->command('snitch:scan')
      ->dailyAt($this->time);
  }
}