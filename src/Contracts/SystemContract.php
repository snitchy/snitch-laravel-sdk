<?php
namespace Snitches\Contracts;

interface SystemContract
{
  public function getOperatingSystem(): string;

  public function getDistribution(): DistributionContract;
}
