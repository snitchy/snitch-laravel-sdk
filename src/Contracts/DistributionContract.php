<?php

namespace Snitches\Contracts;

interface DistributionContract
{
  public function getDistributionName();

  public function packageManager();

  public function lookupCommand(string $identifier, string $versionRetriever = null);

  public function packageManagerLookup(string $identifier);
}
