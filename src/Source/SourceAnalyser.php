<?php

namespace Snitches\Source;

use PhpParser\Node;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayItem;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\PropertyProperty;
use PhpParser\NodeFinder;

class SourceAnalyser extends Parser
{

  protected $finder;

  protected $results;

  public function __construct($filePath)
  {
    parent::__construct($filePath);

    $this->finder = new NodeFinder();

    $this->getCode()
      ->parse();

    $this->results = new ParsingResults;
  }

  public function setTargets($targets)
  {
    $this->targets = $targets;

    return $this;
  }

  public function analyse()
  {
    $nodeType = $this->getNodeType($this->targets->type);

    $occurrences = $this->find(
      $this->parsed,
      $nodeType,
      $this->targets
    );

    $this->results->setPrimaryFindings($this->extractFindings($this->targets, $occurrences));

    $this->results->getPrimaryFindings()->each(function($finding) {
      collect($this->targets->children ?? [])->each(function($target) use($finding) {
        $occurrences = $this->find(
          $finding->finding->node, 
          $this->getNodeType($target->type),
          $target
        );
        $finding->setChildFindings(
          new ParsingResult($this->extractFindings($target, $occurrences))
        );
      });
    });

    return $this->results->prepare()->results;
  }

  public function find($code, $nodeType, $target = null)
  {
    return $this->finder->find($code, function(Node $node) use($nodeType, $target) {

      if(
        $node instanceof $nodeType['handler']
        && ($target && array_search('name', $node->getSubNodeNames()) >= 0)
      ) {
        if(isset($node->name) && is_string($node->name)) {
          return $node->name === $target->identifier->name;
        }
        return $node->name->name === $target->identifier->name;
      }
      return $node instanceof $nodeType['handler'];
    });
  }

  public function extractFindings($target, $nodes = [])
  {
    return collect($nodes)->flatMap(function(Node $originalNode) use($target) {
      $extractor = $this->getExtractor($target->extracts);
      if($extractor['handler_type'] === 'node_search') {
        $nodes = $this->find(
          $originalNode, 
          $extractor,
          $extractor['takes_target'] ? $target : null
        );

        if($extractor['type'] === 'lineNumber') {
          return collect($nodes)->map(function(Node $node) use($target, $originalNode) {
            return (object) [
              'type' => $target->type,
              'identifier' => $target->identifier->name,
              'lineNumber' => $node->getLine(),
              'node' => $originalNode
            ];
          });
        }
        
        if($extractor['type'] === 'arrayItems') {
          return collect([
            (object) [
              'type' => $target->type,
              'identifier' => $target->identifier->name,
              'arrayItems' => collect($nodes)->map(function(Node $node) {
                return $node->value->value;
              })->toArray(),
              'node' => $originalNode
            ]
          ]);
        }
        
        if($extractor['type'] === 'array') {
          return collect([
            (object) [
              'type' => optional($target)->type,
              'identifier' => $target->identifier->name,
              'arrayItems' => collect($nodes)->map(function(Node $node) {
                return $node->items;
              })->toArray(),
              'node' => $originalNode
            ]
          ]);
        }
        
      }
      if($extractor['handler_type'] === 'node_method') {
        return [
          $extractor->type => $originalNode->{$extractor['handler']}()
        ];
      }
      
    })->toArray();
  }

  public function getNodeType($statement)
  {
    return collect([
      [
        'type' => 'method_call',
        'handler' => MethodCall::class,
      ],
      [
        'type' => 'static_method_call',
        'handler' => StaticCall::class,
      ],
      [
        'type' => 'class_method',
        'handler' => ClassMethod::class,
      ],
      [
        'type' => 'property',
        'handler' => PropertyProperty::class,
      ],
      [
        'type' => 'array',
        'handler' => Array_::class,
      ],
      [
        'type' => 'array_items',
        'handler' => ArrayItem::class,
      ],
      [
        'type' => 'identifier',
        'handler' => Identifier::class,
        'attribute' => 'name'
      ],
    ])->firstWhere('type', $statement);
  }

  public function getExtractor($extraction)
  {
    return collect([
      [
        'type' => 'arrayItems',
        'handler_type' => 'node_search',
        'handler' => ArrayItem::class,
        'takes_target' => false
      ],
      [
        'type' => 'array',
        'handler_type' => 'node_search',
        'handler' => Array_::class,
        'takes_target' => false
      ],
      [
        'type' => 'lineNumber',
        'handler_type' => 'node_search',
        'handler' => Identifier::class,
        'takes_target' => true
      ],
    ])->firstWhere('type', $extraction->type);
  }

  public function getResults()
  {
    return $this->results;
  }
}