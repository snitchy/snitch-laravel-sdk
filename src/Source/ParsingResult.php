<?php

namespace Snitches\Source;

class ParsingResult
{
  public $finding;

  public $childFindings;

  public function __construct($finding)
  {
    $this->finding = $finding;
  }

  public function setChildFindings($findings)
  {
    $this->childFindings = $findings;
  }

  public function __toString()
  {
    unset($this->finding->node);
    return json_encode($this);
  }

}