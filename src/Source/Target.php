<?php

namespace Snitches\Source;

class Target
{
  public $identifier;

  public $extracts;

  public function __construct($identifier = null, $extracts = null)
  {
    $this->identifier = new Identifier($identifier);
    $this->extracts = new Extractor($extracts);
  }

  public function __toString()
  {
    return json_encode($this);
  }
}