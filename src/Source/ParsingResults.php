<?php

namespace Snitches\Source;

class ParsingResults
{
  public $results;

  public function __construct()
  {
    $this->results = collect();
  }

  public function setPrimaryFindings($findings)
  {
    foreach($findings as $finding) {
      $this->results->push(
        new ParsingResult($finding)
      );
    }
    return $this;
  }

  public function getPrimaryFindings()
  {
    return $this->results;
  }

  public function prepare()
  {
    $this->results = $this->results->map(function($result) {
      $this->clearOutNodes($result);
      return json_encode($result);
    });
    return $this;
  }

  private function clearOutNodes($result)
  {
    unset($result->finding->node);
    if($result->childFindings) {
      foreach($result->childFindings->finding as $finding) {
        unset($finding->node);
      }
    }
  }

  public function __toString()
  {
    return json_encode($this);
  }
  
}