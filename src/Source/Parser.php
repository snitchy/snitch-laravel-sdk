<?php

namespace Snitches\Source;

use Illuminate\Support\Facades\Log;
use PhpParser\ParserFactory;
use PhpParser\Error;

class Parser
{
  protected $code;

  protected $filePath;

  protected $parsed;

  public function __construct($filePath)
  {
    $this->filePath = $filePath;
  }

  public function getCode()
  {
    $this->code = file_get_contents($this->filePath);
    
    return $this;
  }

  public function parse()
  {
    $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);

    try {
      $this->parsed = $parser->parse($this->code);
    } catch (Error $e) {
      Log::error($e->getMessage());
    }
  }

  public function getParsed()
  {
    return $this->parsed;
  }
}