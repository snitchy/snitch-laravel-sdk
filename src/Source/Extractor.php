<?php

namespace Snitches\Source;

class Extractor
{
  public $type;

  public function __construct($type = null)
  {
    $this->type = $type;
  }

  public function __toString()
  {
    return json_encode($this);
  }
}