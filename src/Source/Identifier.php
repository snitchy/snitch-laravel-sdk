<?php

namespace Snitches\Source;

class Identifier
{
  public $name;

  public function __construct($name = null)
  {
    $this->name = $name;
  }

  public function __toString()
  {
    return json_encode($this);
  }
}