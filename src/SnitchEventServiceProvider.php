<?php

namespace Snitches;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Failed;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Laravel\Passport\Events\AccessTokenCreated;
use Snitches\Events\CaughtBadPassword;
use Snitches\Events\Idor;
use Snitches\Events\UnauthorisedAccess;
use Snitches\Listeners\LogApiAuthentication;
use Snitches\Listeners\LogAuthentication;
use Snitches\Listeners\LogAuthenticationAttempt;
use Snitches\Listeners\LogBruteForce;
use Snitches\Listeners\LogCaughtBadPassword;
use Snitches\Listeners\LogFailedAuthentication;
use Snitches\Listeners\LogIdor;
use Snitches\Listeners\LogPasswordReset;
use Snitches\Listeners\LogUnauthorisedAccess;

class SnitchEventServiceProvider extends EventServiceProvider
{

  protected $listen = [
    Idor::class => [
      LogIdor::class
    ],
    Lockout::class => [
      LogBruteForce::class
    ],
    Login::class => [
      LogAuthentication::class
    ],
    PasswordReset::class => [
      LogPasswordReset::class
    ],
    Failed::class => [
      LogFailedAuthentication::class
    ],
    Attempting::class => [
      LogAuthenticationAttempt::class
    ],
    AccessTokenCreated::class => [
      LogApiAuthentication::class
    ],
    UnauthorisedAccess::class => [
      LogUnauthorisedAccess::class
    ],
    CaughtBadPassword::class => [
      LogCaughtBadPassword::class
    ]
  ];

  public function boot()
  {
    parent::boot();
  }
}
