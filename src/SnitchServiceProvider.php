<?php
namespace Snitches;

use Illuminate\Support\ServiceProvider;
use Snitches\Commands\DecryptHash;
use Snitches\Commands\ImportPasswordList;
use Snitches\Commands\Install;
use Snitches\Commands\RunScan;
use Snitches\Schedule\Scheduler;

class SnitchServiceProvider extends ServiceProvider
{

  public function register()
  {
    $this->app->register(SnitchEventServiceProvider::class);

    $this->app->bind('snitch', function () { return new Snitch; });

    $this->app->config['filesystems.disks.snitch'] = [
      'driver' => 'local',
      'root' => storage_path('app/snitch'),
  ];
  }

  public function boot()
  {
    $this->publishes([
      __DIR__ . '/config.php' => config_path('snitch.php')
    ], 'snitch-config');

    $this->app->booted(function () { new Scheduler(); });

    $this->commands([
      RunScan::class,
      Install::class,
      ImportPasswordList::class,
      DecryptHash::class
    ]);
  }

}