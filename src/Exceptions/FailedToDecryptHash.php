<?php

namespace Snitches\Exceptions;

use Exception;

class FailedToDecryptHash extends Exception
{
  public function __construct()
  {
    parent::__construct($this->getMessage());
  }
}
