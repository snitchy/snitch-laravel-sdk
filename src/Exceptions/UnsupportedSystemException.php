<?php
namespace Snitches\Exceptions;

use Exception;

class UnsupportedSystemException extends Exception
{
  public function __construct(string $systemName)
  {
    parent::__construct("$systemName is not supported by Snitch.");
  }
}
