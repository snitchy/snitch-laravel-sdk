<?php

namespace Snitches\Exceptions;

use Exception;

class EnvironmentNotConfigured extends Exception
{
  public function __construct()
  {
    parent::__construct("Snitch Environment variables missing. Please add the provided configuration variables to your .env file.");
  }
}
