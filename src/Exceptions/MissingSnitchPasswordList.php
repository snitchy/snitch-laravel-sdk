<?php

namespace Snitches\Exceptions;

use Exception;

class MissingSnitchPasswordList extends Exception
{
  public function __construct()
  {
    parent::__construct('You must download the Snitch password list before you can use this validation rule. Run php artisan snitch:import-password-list to import the list.');
  }
}
