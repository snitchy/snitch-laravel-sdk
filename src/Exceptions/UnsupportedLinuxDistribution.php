<?php

namespace Snitches\Exceptions;

use Exception;

class UnsupportedLinuxDistribution extends Exception
{
  public function __construct(string $distributionName)
  {
    parent::__construct("$distributionName is not supported by Snitch.");
  }
}
