<?php

namespace Snitches\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CaughtBadPassword
{
  use SerializesModels, Dispatchable;

  public $password;

  public function __construct($password)
  {
    $this->password = $password;
  }
}
