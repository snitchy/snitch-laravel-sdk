<?php

namespace Snitches\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Idor
{
  use SerializesModels, Dispatchable;

  public $ipAddress;

  public $userId;

  public $url;

  public function __construct($userId, $ipAddress, $url)
  {
    $this->userId = $userId;
    $this->ipAddress = $ipAddress;
    $this->url = $url;
  }
}
