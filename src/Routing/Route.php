<?php

namespace Snitches\Routing;

class Route
{
  public $method;

  public $name;

  public $uri;

  public $controller;

  public $action;

  public $middleware;

  public $controller_middleware;

  public $bindings;

  public $validation_rules;

  public function __toString()
  {
    return json_encode($this);
  }
}