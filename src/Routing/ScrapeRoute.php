<?php

namespace Snitches\Routing;

use Closure;
use Illuminate\Routing\Route as LaravelRoute;
use Snitches\Routing\Route;
use Snitches\Support\Route\ControllerMethod;
use Snitches\Support\Route\RouteBindings;
use Snitches\Support\Route\ValidationRules;

class ScrapeRoute
{

  protected $appRoute;

  protected $method;

  public $route;

  public function __construct(LaravelRoute $appRoute, string $method)
  {
    $this->appRoute = $appRoute;
    $this->method = $method;
    $this->route = new Route;
  }

  public function scrape()
  {
    $this->route->method = $this->method;
    $this->route->name = $this->appRoute->getName();
    $this->route->uri = $this->appRoute->uri();
    $this->route->action = $this->routeAction();
    $this->route->middleware = $this->middleware();
    $this->route->controller_middleware = $this->controllerMiddleware();
    $this->route->controller = $this->controllerClass();
    $this->route->bindings = $this->bindings();
    $this->route->validation_rules = $this->validationRules();

    return $this->route;
  }

  private function isClosure()
  {
    return $this->appRoute->action['uses'] instanceof Closure;
  }

  private function routeAction()
  {
    return $this->isClosure() ? null : $this->controllerMethod();
  }

  public function controllerMethod()
  {
    return ControllerMethod::parse($this->appRoute->action['uses']);
  }

  public function middleware()
  {
    return $this->appRoute->middleware();
  }
  
  public function controllerMiddleware()
  {
    return $this->appRoute->controllerMiddleware();
  }

  public function controllerClass()
  {
    return $this->isClosure() ? null : get_class($this->appRoute->getController());
  }

  public function requiresValidation()
  {
    return in_array($this->method, [
      'POST',
      'PUT',
      'DELETE'
    ]);
  }

  public function bindings()
  {
    return RouteBindings::getBindings($this->controllerClass(), $this->routeAction());
  }

  public function validationRules()
  {
    return $this->requiresValidation() 
      ? $this->extractValidationRules() 
      : [];
  }

  public function extractValidationRules()
  {
    return (new ValidationRules(
      $this->bindings(),
      $this->controllerClass(),
      $this->controllerMethod()
    ))->getRules();
  }
}