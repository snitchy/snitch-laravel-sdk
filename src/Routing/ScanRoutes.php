<?php

namespace Snitches\Routing;

use Illuminate\Routing\Router;
use Snitches\Support\SnitchKernel;
use Closure;
use Exception;
use Illuminate\Support\Facades\Log;
use Snitches\Support\Route\ControllerMethod;
use Snitches\Support\Route\RouteBindings;
use Snitches\Support\Route\ValidationRules;
use Snitches\Wire;

class ScanRoutes
{

  public $routes;

  public $results;

  private $api;

  private $notifier;

  public function __construct(Wire $api)
  {
    $this->api = $api;
    $this->routes = collect(app(Router::class)->getRoutes()->getRoutes());
    $this->middleware = app(SnitchKernel::class)->getRouteMiddleware();
    $this->results = collect();
  }

  public function scan(Closure $callback)
  {
    $this->notifier = $callback;

    $this->notify('comment', "Assessing {$this->routes->count()} Application Routes");

    $this->buildFor('POST')
      ->buildFor('GET')
      ->buildFor('PUT')
      ->buildFor('DELETE');

    $this->notify('info', 'Uploading routing results');

    $this->api->postRoutes([
      'middleware' => $this->middleware,
      'routes' => $this->results->toArray()
    ]);
  }

  private function notify($type, $message)
  {
    $notifer = $this->notifier;
    $notifer([
      'type' => $type, 
      'message' => $message
    ]);
  } 

  private function buildRoute($route, $method)
  {
    try {
      $this->results->push(
        json_encode((new ScrapeRoute($route, $method))->scrape())
      );
    } catch(Exception $e) {
      // Silently catch an exception.
    }
  }

  public function buildFor($method)
  {
    $this->routes->filter(function($route) use($method) {
      return in_array($method, $route->methods);
    })->each(function($route) use($method) {
      $this->buildRoute($route, $method);
    });
    return $this;
  }

  public function __toString()
  {
    return json_encode($this);
  }
}