<?php

namespace Snitches\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Snitches\Wire;

class LogSecurityEvent implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $payload;

  /**
   * Create a new job instance.
   *
   * @param $payload
   * @return voidno it
   */
  public function __construct($payload)
  {
    $this->payload = $payload;
  }

  public function handle()
  {
    (new Wire)->logSecurityEvent($this->payload);
  }
}
