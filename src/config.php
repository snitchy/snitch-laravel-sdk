<?php

return [
  
  /*
  * Snitch
  *
  */

  'secret' => env('SNITCH_SECRET', null),

  'id' => env('SNITCH_ID', null),

  'key' => env('SNITCH_KEY', null),

  /*
  * Time of the day to run a scan. The scan will run in your time zone.
  * 
  * 00:00 -> 23:59
  */
  'scan_time' => env('SNITCH_SCAN_TIME', null),

  /*
  * Event Queue
  *
  * The name of the queue that snitch events will be run on.
  */
  'event_queue' => env('SNITCH_QUEUE', 'default'),

  'queue_connection' => 'default'
];
