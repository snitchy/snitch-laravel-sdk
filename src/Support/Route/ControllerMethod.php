<?php
namespace Snitches\Support\Route;

class ControllerMethod
{
  static public function parse($uses)
  {
    $elements = explode('@', $uses);
    return count($elements) > 1 ? $elements[1] : null;
  }
}