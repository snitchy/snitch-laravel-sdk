<?php

namespace Snitches\Support\Route;

use Exception;
use ReflectionClass;
use Snitches\Source\SourceAnalyser;
use Snitches\Source\Target;

class ValidationRules
{
  protected $bindings;

  protected $class;

  protected $method;

  protected $sourceAnalyser;

  public function __construct(array $bindings, string $class = null, string $method = null)
  {
    $this->bindings = collect($bindings);
    $this->class = $class;
    $this->method = $method;
  }

  public function getRules()
  {
    if($this->hasFormRequest()) {
      return $this->formRequestRules();
    }

    if(!$this->class || !$this->method) {
      return [];
    }

    return $this->lookForControllerValidation();
  }

  public function hasFormRequest()
  {
    return $this->bindings->filter(function ($binding) {
      return $binding['type'] === 'form_request';
    })->count() > 0;
  }

  public function getFormRequest()
  {
    return $this->bindings->filter(function ($binding) {
      return $binding['type'] === 'form_request';
    })->first();
  }

  public function formRequestRules()
  {
    $request = $this->getFormRequest();
    try {
      return (new $request['binding'])->rules();
    } catch(Exception $e) {
      return null;
    }
  }

  public function lookForControllerValidation()
  {
    $this->sourceAnalyser = new SourceAnalyser($this->getClassFile());

    return $this->lookForValidators(
      $this->parseControllerMethod()
    );
  }

  public function getClassFile()
  {
    return (new ReflectionClass($this->class))->getFileName();
  }

  public function parseControllerMethod()
  {
    return $this->sourceAnalyser->find(
      $this->sourceAnalyser->getParsed(),
      $this->sourceAnalyser->getNodeType('class_method'),
      new Target($this->method)
    );
  }

  public function lookForValidators($code)
  {
    $staticValidator = new Target('make', 'array');

    $results = $this->sourceAnalyser->find(
      $code,
      $this->sourceAnalyser->getNodeType('static_method_call'),
      $staticValidator
    );

    if(count($results)) {
      return $this->extractParsedRules($staticValidator, $results);
    }

    $requestValidator = new Target('validate', 'array');

    $results = $this->sourceAnalyser->find(
      $code,
      $this->sourceAnalyser->getNodeType('method_call'),
      $requestValidator
    );

    return $this->extractParsedRules($requestValidator, $results);
  }

  public function extractParsedRules($target, $code)
  {
    $nodes = $this->sourceAnalyser->extractFindings($target, $code);
    
    return collect($nodes)->flatMap(function($node) {
      return collect($node->arrayItems)->flatMap(function($item) {
        return $item;
      });
    })->map(function($item) {
      return [
        $item->key->value => $item->value->value
      ];
    })->toArray();
  }

}