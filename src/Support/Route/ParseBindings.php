<?php

namespace Snitches\Support\Route;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Snitches\Support\Reflector;
use ReflectionParameter;

class ParseBindings
{
  protected $parameters;

  private $reflector;
  
  public function __construct(array $parameters)
  {
    $this->reflector = new Reflector;
    $this->parameters = collect($parameters);
  }

  public function getBindings()
  {
    return $this->parameters->map(function(ReflectionParameter $parameter) {
      return [
        'binding' => $this->getBinding($parameter) ?? $parameter->name,
        'type' => $this->getBindingType($parameter),
        'meta' => $this->getBindingType($parameter) === 'model' ? $this->getModelAttributes($parameter) : []
      ];
    })->toArray();
  }

  public function getBinding($parameter)
  {
    return $this->reflector->getParameterClassName($parameter);
  }

  public function getBindingType($parameter)
  {
    if($this->reflector->isParameterSubclassOf($parameter, Model::class)) {
      return 'model';
    }
    
    if($this->reflector->isParameterSubclassOf($parameter, FormRequest::class)) {
      return 'form_request';
    }
  }

  public function getModelAttributes($parameter)
  {
    $modelClass = $this->getBinding($parameter);
    $model = new $modelClass;

    return [
      'fillable' => $model->getFillable(),
      'guarded' => $model->getGuarded(),
    ];
  }

}
