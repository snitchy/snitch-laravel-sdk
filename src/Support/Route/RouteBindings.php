<?php 

namespace Snitches\Support\Route;

use Snitches\Support\Reflection\GetParameters;

class RouteBindings
{
  static public function getBindings($class = null, $method = null)
  {
    if(!$class || !$method) return null;
    
    return (new ParseBindings(
      (new GetParameters($class, $method))->getParameters()
    ))->getBindings();
  }
}