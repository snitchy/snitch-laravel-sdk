<?php

namespace Snitches\Support\Reflection;

use Illuminate\Support\Facades\Log;
use ReflectionMethod;

class GetParameters
{
  protected $methodReflector;

  public function __construct(string $class, $methodName)
  {
    $this->methodReflector = new ReflectionMethod($class, $methodName);
  }

  public function getParameters()
  {
    return $this->methodReflector->getParameters();
  }
}