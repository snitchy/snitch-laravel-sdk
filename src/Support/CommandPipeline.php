<?php

namespace Snitches\Support;

use League\Pipeline\PipelineBuilder;
use League\Pipeline\PipelineInterface;
use League\Pipeline\ProcessorInterface;
use Snitches\Commands\RunScan;
use Snitches\Dto\Scan;

abstract class CommandPipeline
{
  protected $pipeline;

  protected $command;

  public function __construct(RunScan $command = null)
  {
    $this->command = $command;
    $this->pipeline = new PipelineBuilder;
  }

  /** 
   * @return \League\Pipeline\ProcessorInterface
   */
  public function build()
  {
    return $this->pipeline->build();
  }

  public function process($processable)
  {
    return $this->build()->process($processable);
  }
}