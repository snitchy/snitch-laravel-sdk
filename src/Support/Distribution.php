<?php

namespace Snitches\Support;

use Illuminate\Support\Facades\Log;
use Snitches\Contracts\DistributionContract;
use Snitches\Exceptions\UnsupportedLinuxDistribution;
use Snitches\Support\Distributions\Debian;
use Snitches\Support\Distributions\Fedora;

class Distribution
{
  protected $os_release;

  protected $distribution_name;

  protected $distribution_handler;

  public function __construct(string $osRelease)
  {
    $this->os_release = $osRelease;
    $this->parseIdLike();
    $this->getDistributionHandler();
  }

  public function getDistribution()
  {
    throw_unless(
      $this->distributionIsSupported(),
      new UnsupportedLinuxDistribution($this->distribution_name)
    );

    return new $this->distribution_handler;
  }

  public function supportedDistributions()
  {
    return collect([
      '/fedora$/i' => Fedora::class,
      '/debian$/i' => Debian::class,
    ]);
  }

  private function distributionIsSupported()
  {
    return $this->supportedDistributions()
      ->filter(function ($handler, $distribution) {
        return preg_match($distribution, trim($this->distribution_name, '"'));
      })->count() > 0;
  }

  private function parseIdLike()
  {
    $this->distribution_name = $this->os_release;
  }

  private function getDistributionHandler()
  {
    return $this->distribution_handler = $this->supportedDistributions()
      ->filter(function ($handler, $distribution) {
        return preg_match($distribution, $this->distribution_name);
      })->first();
  }
}