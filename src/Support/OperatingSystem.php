<?php
namespace Snitches\Support;

use Snitches\Support\System;

class OperatingSystem
{
  private $system;

  public function __construct()
  {
    $this->system = System::determine()->get();
  }

  public function getOperatingSystemName(): string
  {
    return $this->system->getOperatingSystem();
  }
  
  public function getDistributionName(): string
  {
    return $this->system->getDistribution()->getDistributionName();
  }

  public function lookupCommand(string $identifier, string $versionRetriever = null)
  {
    return $this->system->getDistribution()->lookupCommand($identifier, $versionRetriever);
  }

  public function packageManagerLookup(string $identifier)
  {
    return $this->system->getDistribution()->packageManagerLookup($identifier);
  }
}
