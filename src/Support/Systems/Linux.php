<?php
namespace Snitches\Support\Systems;

use Snitches\Contracts\DistributionContract;
use Snitches\Contracts\SystemContract;
use Snitches\Support\Distribution;
use Symfony\Component\Process\Process;

class Linux implements SystemContract
{
  public function getOperatingSystem(): string
  {
    return 'Linux';
  }

  public function getDistribution(): DistributionContract
  {
    $process = Process::fromShellCommandline(
      'cat /etc/os-release | grep -i "ID_LIKE" | cut -d = -f 2 | tr -d \'"\''
    );
    
    $process->run();

    return $this->parseDistribution(
      $process->getOutput()
    );
  }

  public function parseDistribution(string $output): DistributionContract
  {
    return (new Distribution($output))->getDistribution();
  }
}
