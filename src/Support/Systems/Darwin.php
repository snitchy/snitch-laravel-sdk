<?php
namespace Snitches\Support\Systems;

use Snitches\Contracts\DistributionContract;
use Snitches\Contracts\SystemContract;
use Snitches\Support\Distributions\Darwin as DarwinDistribution;

class Darwin implements SystemContract
{
  protected $distribution;

  public function __construct()
  {
    $this->distribution = new DarwinDistribution();
  }

  public function getOperatingSystem(): string
  {
    return 'OSX';
  }

  public function getDistribution(): DistributionContract
  {
    return $this->distribution;
  }
}
