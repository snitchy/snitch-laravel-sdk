<?php

namespace Snitches\Support;

class EncryptedEventColumns
{
  public static $login = [
    'user_id'
  ];

  public static $api_login = [
    'user_id',
    'client_id'
  ];
  
  public static $attempt_login = [
    'user_id'
  ];

  public static $failed_login = [
    'user_id'
  ];
  
  public static $idor = [
    'user'
  ];

  public static $password_reset = [
    'user_id'
  ];

  public static $unauthorised_access = [
    'user'
  ];

  public static $login_brute_force = [];

}