<?php

namespace Snitches\Support;

use Snitches\Support\System;

class Attributes
{
  private $system;

  public function __construct()
  {
    $this->system = System::determine()->get();
  }

  public function getOperatingSystemName(): string
  {
    return $this->system->getOperatingSystem();
  }
}
