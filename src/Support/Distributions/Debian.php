<?php

namespace Snitches\Support\Distributions;

use Snitches\Contracts\DistributionContract;

class Debian implements DistributionContract
{
  public function getDistributionName(): string
  {
    return 'Debian';
  }

  public function packageManager():? string
  {
    return 'apt';
  }

  public function lookupCommand(string $identifier, string $versionRetriever = null): string
  {
    if ($versionRetriever) {
      return "$identifier $versionRetriever";
    }

    return $this->packageManagerLookup($identifier);
  }

  public function packageManagerLookup(string $identifier)
  {
    return "if ({$this->packageManager()} info {$identifier} | grep \"Installed\" > /dev/null); then {$this->packageManager()} info {$identifier} | grep -Ei \"Version\"; fi";
  }
}
