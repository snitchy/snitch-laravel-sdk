<?php

namespace Snitches\Support\Distributions;

use Snitches\Contracts\DistributionContract;

class Darwin implements DistributionContract
{

  public function getDistributionName()
  {
    return 'Darwin';
  }

  public function packageManager()
  {
    return null;
  }

  public function lookupCommand(string $identifier, string $versionRetriever = null)
  {
    return "$identifier $versionRetriever";
  }
  
  public function packageManagerLookup(string $identifier)
  {
    return "brew list --versions | grep -i $identifier";
  }
}
