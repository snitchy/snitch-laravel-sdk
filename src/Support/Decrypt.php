<?php

namespace Snitches\Support;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class Decrypt
{
  protected $type;

  protected $payload;

  public function __construct($type, $payload)
  {
    $this->type = $type;
    $this->payload = $payload;
  }

  public function decrypt()
  {
    $type = $this->type;
    foreach(EncryptedEventColumns::${$type} as $column) {
      if(!array_key_exists($column, $this->payload)) continue;
      try {
        $this->payload[$column] = Crypt::decryptString($this->payload[$column]);
        if(($column === 'user_id' || $column === 'user') && is_numeric($this->payload[$column])) {
          $this->payload[$column] = $this->attempToMapUser($this->payload[$column]);
        }
      } catch(DecryptException $e) {
        Log::info($type, [$column]);
        // Unable to decrypt value
      }
    }

    return $this->payload;
  }

  public function attempToMapUser($userId)
  {
    return optional(User::find($userId))->email ?? $userId;
  }
}