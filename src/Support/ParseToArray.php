<?php 

namespace Snitches\Support;


class ParseToArray
{
  public function __invoke($payload)
  {
    return json_decode($payload, true);
  }
}