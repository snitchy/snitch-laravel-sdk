<?php

namespace Snitches\Support;

class ExtractArrayKeys
{
  private $scraper;

  public function __construct($scraper)
  {
    $this->scraper = $scraper;
  }

  public function __invoke($value)
  {
    $matched = [];

    foreach($this->scraper->scraper_payload as $key) {
      $matched[$key] = array_key_exists($key, $value) ? $value[$key] : null;
    }

    return $matched;
  }
}
