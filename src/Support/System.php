<?php
namespace Snitches\Support;

use Illuminate\Support\Str;
use Snitches\Contracts\SystemContract;
use Snitches\Exceptions\UnsupportedSystemException;

class System
{
  private $systemName;
  private $system;

  private function __construct()
  {
    $this->systemName = php_uname('s');
    $this->determineSystem();
  }

  public static function determine(): self
  {
    return new static;
  }

  public function determineSystem(): void
  {
    $className = Str::studly($this->systemName);
    $systemClass = Systems::class . "\\$className";

    throw_unless(
      class_exists($systemClass),
      new UnsupportedSystemException($this->systemName)
    );

    $this->system = new $systemClass;
  }

  public function getName(): string
  {
    return $this->systemName;
  }

  public function get(): SystemContract
  {
    return $this->system;
  }

  public function getDistribution()
  {
    return $this->system->getDistribution();
  }
}
