<?php 

namespace Snitches\Support;

use App\Http\Kernel;

class SnitchKernel extends Kernel
{
  public function getRouteMiddleware()
  {
    return array_merge(
      $this->routeMiddleware,
      array_keys($this->middlewareGroups)
    );
  }

  
}