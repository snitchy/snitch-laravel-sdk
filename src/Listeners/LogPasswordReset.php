<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Facades\Snitch;

class LogPasswordReset implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(PasswordReset $event)
  {
    Snitch::logEvent('password_reset', [
      'user_id' => $event->user->id,
      'event_time' => now()
    ]);
  }
  
}