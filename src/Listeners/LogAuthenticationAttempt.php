<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Failed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Facades\Snitch;

class LogAuthenticationAttempt implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(Attempting $event)
  {
    Snitch::logEvent('attempt_login', [
      'user_id' => $event->credentials['username'] ?? $event->credentials['email'],
      'gaurd' => $event->guard,
      'event_time' => now()
    ]);
  }
}