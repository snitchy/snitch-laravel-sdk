<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Facades\Snitch;

class LogAuthentication implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(Login $event)
  {
    Snitch::logEvent('login', [
      'user_id' => $event->user->id,
      'gaurd' => $event->guard,
      'event_time' => now()
    ]);
  }
}