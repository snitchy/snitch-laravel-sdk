<?php 

namespace Snitches\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Events\UnauthorisedAccess;
use Snitches\Facades\Snitch;

class LogUnauthorisedAccess implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(UnauthorisedAccess $event)
  {
    Snitch::logEvent('unauthorised_access', [
      'ip' => $event->ipAddress,
      'url' => $event->url,
      'user' => $event->userId,
      'event_time' => now()
    ]);
  }
}