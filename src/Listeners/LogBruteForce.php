<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\Lockout;
use Snitches\Facades\Snitch;

class LogBruteForce
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(Lockout $event)
  {
    Snitch::logEvent('login_brute_force', [
      'email' => $event->request->email ?? $event->request->username,
      'ip' => $event->request->ip(),
      'event_time' => now()
    ]);
  }
}