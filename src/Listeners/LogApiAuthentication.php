<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Laravel\Passport\Events\AccessTokenCreated;
use Snitches\Facades\Snitch;

class LogApiAuthentication implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(AccessTokenCreated $event)
  {
    Snitch::logEvent('api_login', [
      'user_id' => $event->userId,
      'client_id' => $event->clientId,
      'event_time' => now()
    ]);
  }
}