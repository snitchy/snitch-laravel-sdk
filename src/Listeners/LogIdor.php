<?php 

namespace Snitches\Listeners;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Events\Idor;
use Snitches\Facades\Snitch;

class LogIdor implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(Idor $event)
  {
    Snitch::logEvent('idor', [
      'ip' => $event->ipAddress,
      'url' => $event->url,
      'user' => $event->userId,
      'event_time' => now()
    ]);
  }
}