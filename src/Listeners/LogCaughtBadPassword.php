<?php 

namespace Snitches\Listeners;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Snitches\Events\CaughtBadPassword;
use Snitches\Events\Idor;
use Snitches\Facades\Snitch;

class LogCaughtBadPassword implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(CaughtBadPassword $event)
  {
    Snitch::logEvent('bad_password', [
      'password' => $event->password,
      'event_time' => now()
    ]);
  }
}