<?php 

namespace Snitches\Listeners;

use Illuminate\Auth\Events\Failed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Snitches\Facades\Snitch;

class LogFailedAuthentication implements ShouldQueue
{
  
  public function __construct()
  {
    $this->queue = config('snitch.event_queue');
  }

  public function handle(Failed $event)
  {
    Snitch::logEvent('failed_login', [
      'user_id' => $event->credentials['username'] ?? $event->credentials['email'],
      'gaurd' => $event->guard,
      'event_time' => now()
    ]);
  }
}