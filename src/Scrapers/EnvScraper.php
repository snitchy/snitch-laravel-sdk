<?php

namespace Snitches\Scrapers;

use Illuminate\Support\Facades\Log;

class EnvScraper
{

  public function __invoke($payload)
  {
    return env($payload->scraper_payload, null);
  }

}