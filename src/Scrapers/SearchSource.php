<?php

namespace Snitches\Scrapers;

use Snitches\Searches\SearchCode;

class SearchSource
{

  public function __invoke($payload)
  {
    $files = (new SearchCode($payload->scraper_payload->search))
      ->setBasePath(
        $this->getPath($payload->scraper_payload->base_path)
      )->search();

    return $files;
  }

  private function getPath($pathFunction)
  {
    if(function_exists($pathFunction))  {
      return $pathFunction();
    }
    return $pathFunction;
  }

}