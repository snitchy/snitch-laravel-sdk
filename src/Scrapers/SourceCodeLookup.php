<?php

namespace Snitches\Scrapers;

use Illuminate\Support\Facades\Log;
use Snitches\Source\SourceAnalyser;

class SourceCodeLookup
{
  protected $scraper;

  protected $files;

  public function __construct($scraper, $files)
  {
    $this->scraper = $scraper;
    $this->files = $files;
  }

  public function __invoke($results)
  {
    $findings = $this->files->map(function($file) {
      return [
        'file' => $file,
        'findings' => $this->scrapeSource($file, $this->scraper->scraper_payload)
      ];
    })->filter(function($results) { return count($results['findings']); })
      ->toArray();

    if(count($findings)) {
      $results[] = $findings;
    }
    return $results;
  }

  private function scrapeSource(string $file, $targets)
  {
    $sourceAnalyser = new SourceAnalyser($file);
    $sourceAnalyser->setTargets($targets);

    return $sourceAnalyser->analyse()->toArray();
  }

}