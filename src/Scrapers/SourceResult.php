<?php

namespace Snitches\Scrapers;

class SourceResult
{
  public $files;

  public $payload;

  public function __construct($files, $payload)
  {
    $this->files = $files;
    $this->payload = $payload;
  }
}