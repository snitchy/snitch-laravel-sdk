<?php

namespace Snitches\Scrapers;

class ConfigScraper
{

  public function __invoke($payload)
  {
    return config($payload->scraper_payload, null);
  }

}