<?php

namespace Snitches;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use Exception;
use Throwable;

class Wire
{
  protected $host = 'https://api.runsnitch.io';

  private $key;

  public function __construct()
  {
    $this->key = Config::get('snitch.key');
  }

  public function authenticate()
  {
    $response = $this->baseClient()->request('post', '/oauth/token', [
      'form_params' => [
        'grant_type' => 'client_credentials',
        'client_id' => Config::get('snitch.id'),
        'client_secret' => Config::get('snitch.secret'),
        'scope' => '',
      ]
    ]);

    if($response->getStatusCode() === 200) {
      Cache::put(
        'snitch_token',
        json_decode((string) $response->getBody(), true)['access_token'],
        10000
      );
      return;
    } else {
      throw new Exception('Auth Failed');
    }
  }

  public function postAttributeResults($output)
  {    
    try {
      $response = $this->post(
        "/environment/{$this->key}/attributes",
        ['results' => $output]
      );

      if($response->getStatusCode() === 200) {
        return json_decode($response->getBody());
      }
    } catch(Throwable $e) {

    }
  }
  
  public function postRoutes(array $payload)
  {    
    try {
      $response = $this->post(
        "/environment/{$this->key}/routes",
        $payload
      );

      if($response->getStatusCode() === 200) {
        return json_decode($response->getBody());
      }
    } catch(Throwable $e) {
      return false;
    }
  }
  

  public function postSoftwareVersions($output)
  {    
    try {
      $response = $this->post(
        "/environment/{$this->key}/software", 
        ['results' => $output]
      );
      if($response->getStatusCode() === 200) {
        return json_decode($response->getBody());
      }
    } catch(Throwable $e) {
      return false;
    }
  }

  public function postVulnerabilityVerifications($output)
  {    
    try {
      $response = $this->post(
        "/environment/{$this->key}/verifications", 
        ['results' => $output]
      );
      if($response->getStatusCode() === 200) {
        return json_decode($response->getBody());
      }
    } catch(Throwable $e) {
      return false;
    }
  }

  public function logEvent($payload)
  {
    try {
      $this->post("/environment/{$this->key}/event", $payload);
    } catch(Exception $e) {
      // Silently handle the exception.
    }
  }

  public function importPasswordList()
  {
    try {
      $response = $this->get("/environment/{$this->key}/password-list");

      return $response->getBody()->getContents();
    } catch(Exception $e) {
      // Silently handle the exception.
    }
  }
  
  public function downloadEventHistory($params)
  {
    try {
      $response = $this->post(
        "/environment/{$this->key}/download-events",
        $params
      );

      return $response->getBody()->getContents();
    } catch(Exception $e) {
      // Silently handle the exception.
    }
  }

  private function baseClient()
  {
    return new Client([
      'base_uri' => $this->host,
      'headers' => $this->requestHeaders()
    ]);
  }

  public function requestHeaders()
  {
    return [
      'Authorization' => 'Bearer ' . $this->getToken(),
      'Accept' => 'application/json',
      'Accept-Encoding' => 'gzip'
    ];
  }

  public function post($url, ?array $data)
  {
    $this->authIfNotAuthenticated();
    return $this->baseClient()->request('POST', $url, [
      'form_params' => $data
    ]);
  }

  public function get($url)
  {
    $this->authIfNotAuthenticated();
    return $this->baseClient()->request('GET', $url);
  }
  
  public function getToken()
  {
    return Cache::get('snitch_token');
  }

  public function isAuthenticated(): bool
  {
    return $this->getToken() !== null;
  }

  public function isAuthorized()
  {
    return $this->isAuthenticated() && $this->canReport();
  }

  public function canReport()
  {
    return true;
  }

  public function getHost()
  {
    return $this->host;
  }

  public function loadAuditableAttributes(string $distribution)
  {
    $response = $this->post("/environment/{$this->key}/audit-attributes", [
      'distribution' => $distribution
    ]);

    return json_decode($response->getBody());
  }

  public function loadSoftwareAttributes()
  {
    $response = $this->baseClient()->request('post', "/environment/{$this->key}/software-attributes", []);

    return json_decode($response->getBody());
  }
  
  public function startAssessment()
  {
    $response = $this->baseClient()->request('post', "/environment/{$this->key}/assess", []);

    return json_decode($response->getBody());
  }

  public function authIfNotAuthenticated()
  {
    if (!$this->isAuthenticated()) {
      $this->authenticate();
    }
  }
}