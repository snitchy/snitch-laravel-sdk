<?php

namespace Snitches\Export;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExportEventsToXls implements WithMultipleSheets
{
  use Exportable;
  
  protected Collection $events;

  public function __construct($events)
  {
    $this->events = collect($events);
  }

  public function sheets(): array
  {
    return $this->events->map(function($events, $key) {
      return new EventSheet($key, $events);
    })->toArray();
  }

}