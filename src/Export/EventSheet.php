<?php

namespace Snitches\Export;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Snitches\Support\Decrypt;

class EventSheet implements FromCollection, WithTitle, WithHeadings
{
  private $eventType;

  private $events;

  private $columnNames;

  public function __construct(string $eventType, array $events)
  {
    $this->eventType = $eventType;
    $this->events = $events;
    $this->columnNames = array_keys($this->eventPayload($events[0]));
  }

  public function collection()
  {
    return collect($this->events)->map(function($event) {
      return (new Decrypt($this->eventType, $this->eventPayload($event)))->decrypt();
    });
  }

  public function title(): string
  {
    return $this->eventType;
  }

  public function headings(): array
  {
    return $this->columnNames;
  }

  public function eventPayload($event): array
  {
    return collect([
      'event_time' => $event->event_time,
    ])->merge((array) $event->meta)
      ->toArray();
  }
}
