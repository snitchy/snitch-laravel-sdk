<?php

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

if (!function_exists('console_cmd')) {
  function console_cmd($command)
  {
    $santizedCommand = str_replace([
      'sudo', 'su', 'rm', '-f'
    ], '', $command);

    $process = Process::fromShellCommandline($santizedCommand);
    $process->run();
    if ($process->isSuccessful()) {
      return $process->getOutput();
    } else {
      return null;
    }
  }
}