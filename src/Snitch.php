<?php
namespace Snitches;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Snitches\Events\CaughtBadPassword;
use Snitches\Events\Idor;
use Snitches\Events\UnauthorisedAccess;

class Snitch
{
  protected $wire;

  public function __construct() 
  {
    $this->wire = new Wire();
  }

  public function logEvent($type, $meta)
  {
    $this->wire->logEvent([
      'ip' => Crypt::encryptString(request()->ip()),
      'type' => $type,
      'meta' => $meta
    ]);
    
  }

  public function logIdor($request, $abortCode = null)
  {
    Idor::dispatch($request->user()->id, $request->ip(), $request->fullUrl());
    if($abortCode) {
      abort($abortCode);
    }
  }
  
  public function logUnauthorisedAccess($request, $abortCode = null)
  {
    UnauthorisedAccess::dispatch($request->user()->id, $request->ip(), $request->fullUrl());
    if($abortCode) {
      abort($abortCode);
    }
  }

  public function logBadPasswordCaught($password)
  {
    CaughtBadPassword::dispatch($password);
  }
}