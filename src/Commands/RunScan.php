<?php

namespace Snitches\Commands;

use Illuminate\Console\Command;
use Snitches\Assessment\AssessInstalledSoftware;
use Snitches\Assessment\ScrapeAttributes;
use Snitches\Routing\ScanRoutes;
use Snitches\Support\OperatingSystem;
use Snitches\Wire;

class RunScan extends Command
{

  protected $signature = 'snitch:scan';

  protected $description = 'Runs a scan';

  protected $wire;

  public $operatingSystem;

  private $start_time;

  public function __construct()
  {
    parent::__construct();
    
    $this->start_time = now();
    $this->operatingSystem = new OperatingSystem;
    $this->wire = new Wire;
  }

  public function handle()
  {

    $this->alert('Snitch Environment Scan');
    $this->line('Snitch will perform a local assessment of your applications environment' . PHP_EOL . 'and submit the output for scrutiny.');
    $this->comment('Profiling installed environment');

    $this->line('Operating System: ' . $this->operatingSystem->getOperatingSystemName());
    $this->line('Distribution: ' . $this->operatingSystem->getDistributionName());

    $this->line('Retrieving Attributes');
    $attributes = $this->wire->loadAuditableAttributes(
      $this->operatingSystem->getDistributionName()
    );

    (new ScrapeAttributes($attributes, $this->wire))->scan(function($activity) {
      $this->{$activity['type']}($activity['message']);
    });

    (new ScanRoutes($this->wire))->scan(function($activity) {
      $this->{$activity['type']}($activity['message']);
    });

    (new AssessInstalledSoftware($this->operatingSystem, $this->wire))
      ->scan(function($activity) {
        $this->{$activity['type']}($activity['message']);
      });

    $this->wire->startAssessment();

    $this->line('Peak memory usage: ' . number_format(memory_get_peak_usage(true) / 1000000, 2) . 'MB');
    $this->line('Total run time: '. $this->start_time->diffInSeconds(now()). 's');
    $this->info('Check the Snitch dashboard for your results.');
  }
}
