<?php

namespace Snitches\Commands;

use Illuminate\Console\Command;
use Snitches\Exceptions\EnvironmentNotConfigured;

class Install extends Command
{

  protected $signature = 'snitch:install';

  protected $description = 'Runs a scan';

  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    $this->checkForEnvironmentVariables();

    $this->line('Publishing configuration file');

    $this->call('vendor:publish', ['--tag' => 'snitch-config']);

    if ($this->confirm('Would you like to import the top 10000 exposed password list for password validation?')) {
      $this->call('snitch:import-password-list');
    }

    if ($this->confirm('Would you like to run a scan now?')) {
      $this->call('snitch:scan');
    }
  }

  public function checkForEnvironmentVariables()
  {
    if(!env('SNITCH_ID') || !env('SNITCH_KEY') || !env('SNITCH_SECRET')) {
      throw new EnvironmentNotConfigured();
    }
  }
}
