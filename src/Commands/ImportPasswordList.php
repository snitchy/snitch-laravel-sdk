<?php

namespace Snitches\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Snitches\Wire;

class ImportPasswordList extends Command
{

  protected $signature = 'snitch:import-password-list';

  protected $description = 'Imports commonly exposed password lists for use in password validation.';

  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    $this->checkForEnvironmentVariables();
  }

  public function checkForEnvironmentVariables()
  {
    $wire = new Wire();
    $this->line('Downloading password list...');
    $rawPasswordList = $wire->importPasswordList();

    Storage::disk('snitch')->put('password-list', $rawPasswordList);
    $this->info('Password list downloaded successfully.');
  }
}
