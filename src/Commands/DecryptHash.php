<?php

namespace Snitches\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use Snitches\Exceptions\FailedToDecryptHash;
use Snitches\Wire;

class DecryptHash extends Command
{

  protected $signature = 'snitch:decrypt-hash';

  protected $description = 'Decrypt a hashed value captured by a Snitch event.';

  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    $hash = $this->ask('Hash');
    try {
      $decryptedValue = Crypt::decryptString($hash);
      $this->info('Hash Decrypted');
      $this->line(
        $decryptedValue
      );
    } catch(Exception $e) {
      throw new FailedToDecryptHash();
    }
  }
}
