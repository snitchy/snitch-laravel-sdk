<?php

namespace Snitches\Assessment;

use Snitches\Support\CommandPipeline;
use Snitches\Support\OperatingSystem;
use Symfony\Component\Process\Process;

class GetInstalledVersion extends CommandPipeline
{

  protected $operating_system;

  private $product;

  public function __construct($product, OperatingSystem $operatingSystem)
  {
    parent::__construct();
    $this->product = $product; 
    $this->operating_system = $operatingSystem;
  }

  public function get()
  {
    return [
      'id' => $this->product->id,
      'version' => $this->getVersion()
    ];
  }

  public function getVersion(): string
  {
    $process = Process::fromShellCommandline(
      $this->operating_system->lookupCommand(
        $this->getIdentifier(),
        $this->getVersionRetriever()
      )
    );
    $process->run();

    $output = $process->getOutput();

    if(!$output && method_exists($this->operating_system, 'packageManagerLookup')) {
      $process = Process::fromShellCommandline(
        $this->operating_system->packageManagerLookup($this->getIdentifier())
      );
      $process->run();
  
      $output = $process->getOutput();
    }
    return $output;
  }

  public function getIdentifier(): string
  {
    return collect($this->product->distributions)->first()->distribution->identifier;
  }
  
  public function getVersionRetriever():? string
  {
    return collect($this->product->distributions)->first()->distribution->version_retriever;
  }
}