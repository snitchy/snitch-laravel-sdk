<?php

namespace Snitches\Assessment;

use Closure;
use Snitches\Dto\Scan;
use Snitches\Verification\VerifyVulnerability;
use Snitches\Wire;

class AssessInstalledSoftware
{
  protected $software_attributes;

  protected $vulnerabilities;

  protected $operating_system;

  public $results;

  private $notifier;

  public function __construct($operatingSystem, Wire $wire)
  {
    $this->wire = $wire;
    $this->operating_system = $operatingSystem;
    $this->results = collect();
  }

  public function scan(Closure $callback)
  {
    $this->notifier = $callback;
    $this->loadSoftware();
    $this->software_attributes->each(function($software) {
      $this->notify('line', 'Checking for ' . $software->name);
      $version = (new GetInstalledVersion($software, $this->operating_system))->get();

      if($version['version']) {
        $this->notify('line', $version['version']);
      }
      $this->results->push($version);
    });

    $this->vulnerabilities = collect($this->wire->postSoftwareVersions($this->results->toArray()));
    $this->results = collect();
    $this->verifyVulnerabilities();
  }

  public function loadSoftware()
  {
    $this->notify('info', 'Loading Software Attributes');
    $this->software_attributes = collect(
      $this->wire->loadSoftwareAttributes()
    );
  }

  public function verifyVulnerabilities()
  {
    $this->vulnerabilities->each(function($vulnerability) {
      $this->notify('line', 'Assessing ' . $vulnerability->cvid);
      $this->results->push(
        (new VerifyVulnerability($vulnerability))->verify()
      );
    });

    $this->notify('info', 'Uploading Verifications');
    $this->wire->postVulnerabilityVerifications($this->results->toArray());
  }

  private function notify($type, $message)
  {
    $notifer = $this->notifier;
    $notifer([
      'type' => $type, 
      'message' => $message
    ]);
  } 
}