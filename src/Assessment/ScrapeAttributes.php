<?php

namespace Snitches\Assessment;

use Closure;
use Snitches\Attribute\ScrapeAttributeValue;

class ScrapeAttributes
{
  protected $attributes;

  public $results;

  private $notifier;

  private $wire;

  public function __construct($attributes, $wire)
  {
    $this->attributes = collect($attributes);
    $this->results = collect();
    $this->wire = $wire;
  }

  public function scan(Closure $callback)
  {
    $this->notifier = $callback;

    $this->notify('info', 'Assessing Application Attributes');
    
    $this->attributes->each(function ($attribute) {
      
      $value = (new ScrapeAttributeValue($attribute))->scrape();
      
      $this->notify('line', "Assessing {$attribute->title}");
      
      $this->results->push([
        'value' => $value,
        'id' => $attribute->id
      ]);
    });

    $this->notify('info', 'Uploading Attribute Results');
    $this->wire->postAttributeResults($this->results->toArray());
  }

  private function notify($type, $message = 'false')
  {
    $notifer = $this->notifier;
    $notifer([
      'type' => $type, 
      'message' => $message
    ]);
  } 
}
