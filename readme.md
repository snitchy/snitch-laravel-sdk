<a href="https://gitlab.com/snitchy/snitch-laravel-sdk/-/commits/master">
  <img alt="pipeline status" src="https://gitlab.com/snitchy/snitch-laravel-sdk/badges/master/pipeline.svg" />
</a>

# Snitch Laravel SDK

### Requirements

- You'll need a Snitch account, if you dont already have one you head over to [runsnitch.io](https://runsnitch.io).
- Laravel ^5.3, ^6.0, ^7.0., ^8.0
- If you would like to run scheduled scans, you will need to set up your applications scheduling (cron) service.

**While not required we do encourage the use of queues (redis), if your queues are syncronous threat analysis will be disabled.**

### Get Started

Require the latest version of the Snitch SDK in your Laravel application.

```shell
$ composer require snitchy/snitch-laravel-sdk
```

Add your site credentials to your `.env` file. Site credentials can be generated from your
Snitch account.

```shell
SNITCH_ID=snitch_id
SNITCH_SECRET=snitch_secret_here
SNITCH_SITE_KEY=snitch_site_key_here
```

### Installation

Run the installer to configure and connect your scanner.

```shell
$ php artisan snitch:install
```