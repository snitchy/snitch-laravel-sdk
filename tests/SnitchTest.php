<?php

namespace Tests;

use Snitches\SnitchServiceProvider;
use Orchestra\Testbench\TestCase;

class SnitchTest extends TestCase
{
  protected function setUp(): void
  {
    parent::setUp();
  }

  protected function getPackageProviders($app)
  {
    return [
      SnitchServiceProvider::class
    ];
  }

  protected function getEnvironmentSetUp($app)
  {
    $app['config']->set('snitch', [
      'site_key' => 'QERQWER'
    ]);
  }
}
