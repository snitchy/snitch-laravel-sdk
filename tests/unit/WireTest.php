<?php

use Snitches\Wire;
use Tests\SnitchTest;

final class WireTest extends SnitchTest
{
  public function testWireIsPointingToApi(): void
  {
    
    $wire = new Wire();

    $this->assertEquals(
      $wire->getHost(),
      'https://api.runsnitch.io'
    );
    
  }

}
